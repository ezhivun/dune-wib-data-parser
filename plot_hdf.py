#!/usr/bin/env python3

import numpy as np
import pdb, argparse, os, h5py
import numpy as np
import matplotlib.pyplot as plt
from wib_parser import num_femb, num_col, num_chan, num_adc
from os import path
import math


__author__ = "Elena Zhivun"
__version__ = "0.0.1"
__maintainer__ = "Elena Zhivun"
__email__ = "ezhivun@bnl.gov"


def main():
    global parser
    parser = argparse.ArgumentParser(description="Plot DUNE wib data from a specified hdf5 file")
    parser.add_argument("file_name", help='file to plot', type=str)
    parser.add_argument("-o", "--out_dir", help="Path to the output directory for the plots",
        type=str, required=False, default=".")
    parser.add_argument("-col", "--col", help="FEMB column", type=int,
        default=0, required=False)
    parser.add_argument("-femb", "--femb", help="FEMB ID", type=int,
        default=0, required=False)
    parser.add_argument("-adc", "--adc", help="ADC ID", type=int,
        default=0, required=False)
    parser.add_argument("-ch", "--chan", help="ADC channel", type=int,
        default=0, required=False)
    parser.add_argument("-n", "--num_points", help="Number of points to plot", type=int,
        default=1000, required=False)
    parser.add_argument("-t", "--time", help="How many seconds of data to plot", type=float,
        required=False)
    parser.add_argument("-a", "--all", help="Plot all channels (takes a long time, makes many files)",
        action="store_true", required=False)
    parser.add_argument("-d", "--deltas", help="Plot timestamp deltas", action="store_true",
        required=False)
    
    
    global args
    args = parser.parse_args()

    with h5py.File(args.file_name, 'r') as file:
        if args.deltas:
            plot_timestamps(file)
        test_num_points = min(args.num_points, len(file["master_time_stamp"]))
        num_points, delta = get_time_axis(file, test_num_points)
        if args.all:
            plot_all_data(file, num_points=num_points, delta=delta)
        else:
            verify_adc_arguments()
            plot_data(file, num_points, delta=delta,
                femb=args.femb, col=args.col, adc=args.adc, chan=args.chan)
        

# verify that ADC channel IDs have been correctly specified
def verify_adc_arguments():
    verify_adc_argument(args.femb, min=0, max=num_femb-1, label="--femb ID")
    verify_adc_argument(args.col, min=0, max=num_col-1, label="--col ID")
    verify_adc_argument(args.adc, min=0, max=num_adc-1, label="--adc ID")
    verify_adc_argument(args.chan, min=0, max=num_chan-1, label="--chan ID")


# verify the command line argument specifying an ADC channel ID
def verify_adc_argument(value, min, max, label):
    if value is None:
        parser.error(f"Please specify {label}")
    if value < 0 or value > max:
        parser.error(f"{label} is out of range, allowed values are {min}..{max}")


# determine how many points to put on the plot
def get_time_axis(file, test_num_points):
    dt = get_timestamp_delta(file, test_num_points)
    if args.time is not None:
        dt_num_points = int(args.time * 1e9 / dt)
        max_num_points = len(file["master_time_stamp"])
        if dt_num_points > max_num_points:
            max_data_dt = float(max_num_points * dt * 1e-9)
            print(f"Warning: the hdf5 file only has {max_data_dt:.3e} seconds of data")
        num_points = min(max_num_points, dt_num_points)
    else:
        num_points = test_num_points
    if num_points <= 0:
        raise(parser.error(f"Invalid sample duration is specified: {dt_num_points}"))
    return num_points, dt


# Plot timestamp deltas to verify the time stamps are OK
def plot_timestamps(file):    
    ts = file["master_time_stamp"][0:args.num_points]    
    print(f"Timestamp begins at {ts[0]}")
    grad = np.gradient(ts)
    plt.plot(grad)
    data_file_name = get_data_file_name()
    file_name = path.join(args.out_dir, f"{data_file_name}_ts_diff.png")
    plt.savefig(file_name)
    plt.close()


def get_timestamp_delta(file, test_num_points):
    ts = file["master_time_stamp"][0:test_num_points]
    grad = np.gradient(ts)
    dt = set(grad)
    if len(dt) != 1:
        raise ValueError(f"The timestamp deltas are inconsistent, please check the data format! dt={dt}")
    return dt.pop()


# plot all ADC channels
def plot_all_data(file, num_points, delta):
    for femb in range(num_femb):
        for col in range(num_col):
            for adc in range(num_adc):
                for chan in range(num_chan):
                    plot_data(file, femb=femb, col=col, adc=adc,
                        chan=chan, num_points=num_points)


def plot_data(file, num_points, delta, femb, col, adc, chan):
    data_file_name = get_data_file_name()
    y = file["adc_data"][femb][col][adc][chan][0:num_points]        
    x = np.arange(0, num_points) * delta    
    plt.plot(x, y)
    plt.title(f"{data_file_name}\nFEMB {femb}, column {col}, ADC {adc}, channel {chan}")
    plt.ylabel("ADC data [a.u.]")
    plt.xlabel("Time [ns]")
    plt.ticklabel_format(axis="x", style="sci", scilimits=(0,0))
    file_name = path.join(args.out_dir, f"{data_file_name}_femb{femb}_col{col}_adc{adc}_chan{chan}.png")
    plt.savefig(file_name)
    plt.close()


def get_data_file_name():
    dir, full_file_name = os.path.split(args.file_name)
    file_name, ext = os.path.splitext(full_file_name)
    return file_name


if __name__ == '__main__':
    main()
