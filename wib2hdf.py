#!/usr/bin/env python3

from wib_parser import WIB_Parser, frame_size_bits, frame_size_bytes, ParseError
from wib_parser import num_femb, num_col, num_chan, num_adc
#from wib_parser import parse_adc_data, make_fake_data
import numpy as np
from tqdm import tqdm
from pprint import pprint
import pdb, argparse, os, h5py
from bitstring import ReadError


__author__ = "Elena Zhivun"
__version__ = "0.0.1"
__maintainer__ = "Elena Zhivun"
__email__ = "ezhivun@bnl.gov"


def main():
	parser = argparse.ArgumentParser(description=("Decoding DUNE WIB data captured by FELIX "
		+ " and save it into an HDF5 file"))
	parser.add_argument('file_name', help='file to parse', type=str)
	parser.add_argument("-m", "--max_frames", help='Maximum number of frames to decode',
	 	default=0, type=int, required=False)
	parser.add_argument("-s", "--start_offset", help='Offset of the data frame with respect to the beginning of the file in bytes',
	 	default=0, type=int, required=False)
	parser.add_argument("-f", "--format", help="Try alternative timestamp format",
		action="store_true")
	# parser.add_argument("-f", "--find_header", help='Scan the file to determine possible header offsets',
	# 	action="store_true")
	# parser.add_argument("-f", "--femb_id", help=('Lowest FEMB ID on this link (0 or 2)'),
	# 	default=0, type=int, required=False)
	parser.add_argument("-o", "--output_file_name", help="File name for the output HDF5 file",
		type=str, default="output.hdf5", required=False)

	global args
	args = parser.parse_args()

	if not os.path.isfile(args.file_name):
		print(f"Can't open {args.file_name}")
		return

	wib_parser = WIB_Parser(file=args.file_name, timestamp_format=args.format)

	if args.start_offset:
		print(f"Parsing {args.file_name} starting at {args.start_offset}")
		parse_file(wib_parser, args.start_offset)
	else:
		print(f"Looking for WIB headers in {args.file_name}")
		header_offset = find_header(wib_parser)		
		if header_offset:
			start_offset = header_offset[-1]
			print(f"Possible frame header found at {header_offset}, trying {start_offset}")
			parse_file(wib_parser, start=start_offset)
		else:
			print(f"Can't detect WIB headers in {args.file_name}. Please check your data.")
			print("Did you run `flx-config set DECODING_ENDIANNESS_FULL_MODE=0x1` before taking the data?")




def find_header(wib_parser):
	header_offsets = []	

	try:
		for i in range(0, frame_size_bytes, 4):
			wib_parser.seek_bytes(i)
			if wib_parser.is_next_packet_valid():
				header_offsets.append(i)

	except IndexError:
		print("Reached the end of file")

	return header_offsets


def parse_file(wib_parser, start):

	file_size = os.path.getsize(args.file_name)	
	aux_data_labels = ["master_time_stamp", "min_mismatch", "coldata_time_stamp"]
	num_frames = int((file_size - start) / (frame_size_bits / 8))		

	if args.max_frames != 0 and args.max_frames < num_frames:
		max_frames = args.max_frames
	else:
		max_frames = num_frames

	output = create_hd5_datasets(max_frames)
	si5344_lol = False
	ready = True

# this needs to be re-written
	try:
		wib_parser.seek_bytes(start)
		for i in tqdm(range(0, max_frames), unit="frames"):				
			packet = wib_parser.read_next()
			si5344_lol = packet["si5344_lol"] | si5344_lol
			ready = packet["ready"] & ready				

			if i == 0:					
				label_data(output, packet)

			for label in aux_data_labels:
				output[label][i] = packet[label]
			
			output["adc_data"][:, :, :, : , i] = packet["ADC_data"]

	except ParseError as err:
		print("Corrupted data in the middle of the file: bits that are expected to be zero are not zero")

	except IndexError:
		print("WIB data underflow")

	finally:
		output["file"].attrs["si5344_lol"] = si5344_lol
		output["file"].attrs["ready"] = ready
		output["file"].close()



def label_data(data, packet):
	labels = ["link", "bp_slot_addr", "crate_id", "det_id", "version",
		"link_mask", "femb_val", "cdts_id", "femb_pulser_in_frame",
		"context_fld", "ws", "flex"]

	for label in labels:
		data["file"].attrs[label] = packet[label]


def create_hd5_datasets(num_frames):
	print(f"Creating output file {args.output_file_name}")
	output = h5py.File(args.output_file_name, "w")

	datasets = output.create_dataset("adc_data", 
		(num_femb, num_col, num_adc, num_chan, num_frames,), dtype=np.uint16)
	

	return {
		"file" : output,
		# "femb_groups" : fembs,
		# "col_groups" : groups,
		"adc_data" : datasets,
		"coldata_time_stamp" : output.create_dataset("coldata_time_stamp",
			(num_frames,), dtype=np.uint16),
		"min_mismatch" : output.create_dataset("min_mismatch", (num_frames,),
			dtype=np.uint8),
		"master_time_stamp" : output.create_dataset("master_time_stamp",
			(num_frames,), dtype=np.uint64),
	}


if __name__ == '__main__':
    main()
