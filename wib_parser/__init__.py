from bitarray import bitarray
from bitarray.util import ba2int
import numpy as np
import time, pdb

frame_size = 1792
frame_size_bits = 3776
frame_size_bytes = int(frame_size_bits / 8)

num_femb = 2
num_col = 2
num_adc = 4
num_chan = 16


class ParseError(Exception):
  pass


def make_fake_data():
  data = np.empty(shape=(8,32))
  for i in range(0,8):
    for j in range(32):
      data[i][j] = 2**8 * i + j   

  result = np.empty(shape=(2,2,4,16))
  for i in range(0, 2):
    for j in range(0, 2):
      for k in range(0, 4):
        for l in range(0, 16):
          result[i][j][k][l] = data[i*4 + j*2 + int(k/2)][2**4 * (k % 2) + (l % 16)]

  return result
    


def parse_adc_data_helper(data, result, femb_id):
  # reshuffle 32 bit words
  num_words = int(frame_size / 32)
  for i in range(int(num_words / 2)):
    tmp = data[i*32 : (i+1)*32]
    data[i*32 : (i+1)*32] = data[(num_words-1-i)*32 : (num_words-i)*32]
    data[(num_words-1-i)*32 : (num_words-i)*32] = tmp

  # [femb][coldata][adc][sample]
  result[femb_id][0][1][4] = ba2int(data[1778:1792])
  result[femb_id][0][3][11] = ba2int(data[1764:1778])
  result[femb_id][0][1][3] = ba2int(data[1750:1764])
  result[femb_id][0][3][12] = ba2int(data[1736:1750])
  result[femb_id][0][1][2] = ba2int(data[1722:1736])
  result[femb_id][0][3][13] = ba2int(data[1708:1722])
  result[femb_id][0][1][1] = ba2int(data[1694:1708])
  result[femb_id][0][3][14] = ba2int(data[1680:1694])
  result[femb_id][0][1][0] = ba2int(data[1666:1680])
  result[femb_id][0][3][15] = ba2int(data[1652:1666])
  result[femb_id][0][0][4] = ba2int(data[1638:1652])
  result[femb_id][0][2][11] = ba2int(data[1624:1638])
  result[femb_id][0][0][3] = ba2int(data[1610:1624])
  result[femb_id][0][2][12] = ba2int(data[1596:1610])
  result[femb_id][0][0][2] = ba2int(data[1582:1596])
  result[femb_id][0][2][13] = ba2int(data[1568:1582])
  result[femb_id][0][0][1] = ba2int(data[1554:1568])
  result[femb_id][0][2][14] = ba2int(data[1540:1554])
  result[femb_id][0][0][0] = ba2int(data[1526:1540])
  result[femb_id][0][2][15] = ba2int(data[1512:1526])
  result[femb_id][1][0][4] = ba2int(data[1498:1512])
  result[femb_id][1][2][11] = ba2int(data[1484:1498])
  result[femb_id][1][0][3] = ba2int(data[1470:1484])
  result[femb_id][1][2][12] = ba2int(data[1456:1470])
  result[femb_id][1][0][2] = ba2int(data[1442:1456])
  result[femb_id][1][2][13] = ba2int(data[1428:1442])
  result[femb_id][1][0][1] = ba2int(data[1414:1428])
  result[femb_id][1][2][14] = ba2int(data[1400:1414])
  result[femb_id][1][0][0] = ba2int(data[1386:1400])
  result[femb_id][1][2][15] = ba2int(data[1372:1386])
  result[femb_id][1][1][4] = ba2int(data[1358:1372])
  result[femb_id][1][3][11] = ba2int(data[1344:1358])
  result[femb_id][1][1][3] = ba2int(data[1330:1344])
  result[femb_id][1][3][12] = ba2int(data[1316:1330])
  result[femb_id][1][1][2] = ba2int(data[1302:1316])
  result[femb_id][1][3][13] = ba2int(data[1288:1302])
  result[femb_id][1][1][1] = ba2int(data[1274:1288])
  result[femb_id][1][3][14] = ba2int(data[1260:1274])
  result[femb_id][1][1][0] = ba2int(data[1246:1260])
  result[femb_id][1][3][15] = ba2int(data[1232:1246])
  result[femb_id][0][1][9] = ba2int(data[1218:1232])
  result[femb_id][0][3][6] = ba2int(data[1204:1218])
  result[femb_id][0][1][8] = ba2int(data[1190:1204])
  result[femb_id][0][3][7] = ba2int(data[1176:1190])
  result[femb_id][0][1][7] = ba2int(data[1162:1176])
  result[femb_id][0][3][8] = ba2int(data[1148:1162])
  result[femb_id][0][1][6] = ba2int(data[1134:1148])
  result[femb_id][0][3][9] = ba2int(data[1120:1134])
  result[femb_id][0][1][5] = ba2int(data[1106:1120])
  result[femb_id][0][3][10] = ba2int(data[1092:1106])
  result[femb_id][0][0][9] = ba2int(data[1078:1092])
  result[femb_id][0][2][6] = ba2int(data[1064:1078])
  result[femb_id][0][0][8] = ba2int(data[1050:1064])
  result[femb_id][0][2][7] = ba2int(data[1036:1050])
  result[femb_id][0][0][7] = ba2int(data[1022:1036])
  result[femb_id][0][2][8] = ba2int(data[1008:1022])
  result[femb_id][0][0][6] = ba2int(data[994:1008])
  result[femb_id][0][2][9] = ba2int(data[980:994])
  result[femb_id][0][0][5] = ba2int(data[966:980])
  result[femb_id][0][2][10] = ba2int(data[952:966])
  result[femb_id][1][0][9] = ba2int(data[938:952])
  result[femb_id][1][2][6] = ba2int(data[924:938])
  result[femb_id][1][0][8] = ba2int(data[910:924])
  result[femb_id][1][2][7] = ba2int(data[896:910])
  result[femb_id][1][0][7] = ba2int(data[882:896])
  result[femb_id][1][2][8] = ba2int(data[868:882])
  result[femb_id][1][0][6] = ba2int(data[854:868])
  result[femb_id][1][2][9] = ba2int(data[840:854])
  result[femb_id][1][0][5] = ba2int(data[826:840])
  result[femb_id][1][2][10] = ba2int(data[812:826])
  result[femb_id][1][1][9] = ba2int(data[798:812])
  result[femb_id][1][3][6] = ba2int(data[784:798])
  result[femb_id][1][1][8] = ba2int(data[770:784])
  result[femb_id][1][3][7] = ba2int(data[756:770])
  result[femb_id][1][1][7] = ba2int(data[742:756])
  result[femb_id][1][3][8] = ba2int(data[728:742])
  result[femb_id][1][1][6] = ba2int(data[714:728])
  result[femb_id][1][3][9] = ba2int(data[700:714])
  result[femb_id][1][1][5] = ba2int(data[686:700])
  result[femb_id][1][3][10] = ba2int(data[672:686])
  result[femb_id][0][1][15] = ba2int(data[658:672])
  result[femb_id][0][3][0] = ba2int(data[644:658])
  result[femb_id][0][1][14] = ba2int(data[630:644])
  result[femb_id][0][3][1] = ba2int(data[616:630])
  result[femb_id][0][1][13] = ba2int(data[602:616])
  result[femb_id][0][3][2] = ba2int(data[588:602])
  result[femb_id][0][1][12] = ba2int(data[574:588])
  result[femb_id][0][3][3] = ba2int(data[560:574])
  result[femb_id][0][1][11] = ba2int(data[546:560])
  result[femb_id][0][3][4] = ba2int(data[532:546])
  result[femb_id][0][1][10] = ba2int(data[518:532])
  result[femb_id][0][3][5] = ba2int(data[504:518])
  result[femb_id][0][0][15] = ba2int(data[490:504])
  result[femb_id][0][2][0] = ba2int(data[476:490])
  result[femb_id][0][0][14] = ba2int(data[462:476])
  result[femb_id][0][2][1] = ba2int(data[448:462])
  result[femb_id][0][0][13] = ba2int(data[434:448])
  result[femb_id][0][2][2] = ba2int(data[420:434])
  result[femb_id][0][0][12] = ba2int(data[406:420])
  result[femb_id][0][2][3] = ba2int(data[392:406])
  result[femb_id][0][0][11] = ba2int(data[378:392])
  result[femb_id][0][2][4] = ba2int(data[364:378])
  result[femb_id][0][0][10] = ba2int(data[350:364])
  result[femb_id][0][2][5] = ba2int(data[336:350])
  result[femb_id][1][0][15] = ba2int(data[322:336])
  result[femb_id][1][2][0] = ba2int(data[308:322])
  result[femb_id][1][0][14] = ba2int(data[294:308])
  result[femb_id][1][2][1] = ba2int(data[280:294])
  result[femb_id][1][0][13] = ba2int(data[266:280])
  result[femb_id][1][2][2] = ba2int(data[252:266])
  result[femb_id][1][0][12] = ba2int(data[238:252])
  result[femb_id][1][2][3] = ba2int(data[224:238])
  result[femb_id][1][0][11] = ba2int(data[210:224])
  result[femb_id][1][2][4] = ba2int(data[196:210])
  result[femb_id][1][0][10] = ba2int(data[182:196])
  result[femb_id][1][2][5] = ba2int(data[168:182])
  result[femb_id][1][1][15] = ba2int(data[154:168])
  result[femb_id][1][3][0] = ba2int(data[140:154])
  result[femb_id][1][1][14] = ba2int(data[126:140])
  result[femb_id][1][3][1] = ba2int(data[112:126])
  result[femb_id][1][1][13] = ba2int(data[98:112])
  result[femb_id][1][3][2] = ba2int(data[84:98])
  result[femb_id][1][1][12] = ba2int(data[70:84])
  result[femb_id][1][3][3] = ba2int(data[56:70])
  result[femb_id][1][1][11] = ba2int(data[42:56])
  result[femb_id][1][3][4] = ba2int(data[28:42])
  result[femb_id][1][1][10] = ba2int(data[14:28])
  result[femb_id][1][3][5] = ba2int(data[0:14])
     
# from bitarray.util import ba2hex
def parse_adc_data(data, offset):
  result = np.empty(shape=(2,2,4,16)) # [femb][coldata][adc][sample]
  parse_adc_data_helper(data[offset : offset + frame_size], result, femb_id=0)
  parse_adc_data_helper(data[offset + frame_size : offset + frame_size*2], result, femb_id=1)
  return result


# The parser itself
class WIB_Parser():
  def __init__(self, file, pos=0, timestamp_format=False):
    self.data = bitarray()
    with open(file, 'rb') as f:
      self.data.fromfile(f)

    self.length = len(self.data)
    self.parse_errors = 0
    self.packets_total = 0
    self.timeout = False
    self.frame_size = frame_size_bits # bits
    self.timestamp_format = timestamp_format
    self.pos = pos


  def skip_bytes(self, bytes_to_skip):
    self.pos += bytes_to_skip * 8


  def seek_bits(self, pos):
    self.pos = pos


  def seek_bytes(self, pos_bytes):
    self.pos = pos_bytes * 8


  # Capture and parse the next packet
  # compare to https://github.com/madorskya/wib_sim/blob/master/wib_zu6cg/wib_zu6cg.srcs/sources_1/imports/sim_1/wib/frame_builder_single.sv
  # def get_next_packet(self):
  #   p = self.pos
  #   packet = {

  #     "version" : ba2int(self.data[p+0 : p+5]),
  #     "det_id" : ba2int(self.data[p+6 : p+11]),
  #     "crate_id" : ba2int(self.data[p+12 : p+21]),      
  #     "bp_slot_addr" : ba2int(self.data[p+22 : p+25]), 
  #     "link" : ba2int(self.data[p+26 : p+31]),
  #     "channel" : ba2int(self.data[p+32 : p+34]),
  #     "crc_error" : ba2int(self.data[p+35 : p+36]),
  #     "link_mask" : ba2int(self.data[p+37 : p+38]),
  #     "si5344_lol" : bool(self.data[p+39]),
  #     "ws" : bool(self.data[p+40]),
  #     "femb_sync" : self.data[p+41 : p+42],
  #     "pulser" : self.data[p+43 : p+43],
  #     "psr_cal" : bool(self.data[p+44]),
  #     "ready" : bool(self.data[p+45]),
  #     "context_fld" : ba2int(self.data[p+46 : p+53]),
  #     "zero_h0" : self.data[p+54 : p+63],
  #     "zero_h1" : self.data[p+64 : p+127],
      
  
  #     "master_time_stamp" : ba2int(self.data[p+127 : p+191]),
  #     "zero_h3" : self.data[p+192 : p+223],
  #     "time16_reclocked_1" : ba2int(self.data[p+224 : p+239]),
  #     "time16_reclocked_0" : ba2int(self.data[p+240 : p+255]),

  #     "ADC_data" : parse_adc_data(self.data, p+256),
  #   }

  #   return packet


  # this is WIB data format from February 2022
  def get_next_packet(self):
      p = self.pos
      packet = {
        "link" : ba2int(self.data[p : p+6]), #.read(6),
        "bp_slot_addr" : ba2int(self.data[p+6 : p+10]), #.read(4),
        "crate_id" : ba2int(self.data[p+10 : p+20]), #.read(10),
        "det_id" : ba2int(self.data[p+20 : p+26]), #.read(6),
        "version" : ba2int(self.data[p+26 : p+32]), #.read(6),
        
        #"timestamp_reclocked_lsb" : self.data[p+32 : p+64], #.read(32),
        #"timestamp_reclocked_msb" : self.data[p+64 : p+96], #.read(32),
        #"master_time_stamp" : ba2int(self.data[p+32 : p+64] + self.data[p+64 : p+96]),
        
        #"master_time_stamp" : ba2int(self.data[p+32 : p+96]),

        "zero_h4_start" : self.data[p+96 : p+101], #.read(5),      
        "si5344_lol" : bool(self.data[p+101]), #.read(1),
        "link_mask" : ba2int(self.data[p+102 : p+110]), #.read(8),
        "femb_val" : ba2int(self.data[p+110 : p+112]), #.read(2),
        "cdts_id" : ba2int(self.data[p+112 : p+115]), #.read(3),
        "zero_h4_end" : self.data[p+115 : p+128], #.read(13),

        "zero_h5_start" : self.data[p+128 : p+129], #.read(1),
        "coldata_time_stamp" : ba2int(self.data[p+129 : p+144]), #.read(15),
        "min_mismatch" : ba2int(self.data[p+144 : p+152]), #.read(8),
        "femb_pulser_in_frame" : ba2int(self.data[p+152 : p+160]), #.read(8),

        #"ADC_data" : parse_adc_data(self.data[p+160 : p+3744]), #.read(3584),
        "ADC_data" : parse_adc_data(self.data, p+160),      

        "zero_t0_start" : self.data[p+3744 : p+3745], #.read(1),
        "context_fld" : ba2int(self.data[p+3745 : p+3753]), #.read(8),
        "ready" : bool(self.data[p+3753]), #.read(1),
        "psr_cal" : ba2int(self.data[p+3754 : p+3758]), #.read(4),
        "ws" : bool(self.data[p+3758]), #.read(1),
        "zero_t0_end" : self.data[p+3759 : p+3760], #.read(1),
        "flex" : ba2int(self.data[p+3760 : p+3776]), #.read(16),
      }

      if not self.timestamp_format:
        packet["master_time_stamp"] = ba2int(self.data[p+64 : p+96] + self.data[p+32 : p+64])
      else:
        packet["master_time_stamp"] = ba2int(
            self.data[p+88 : p+96] + 
            self.data[p+80 : p+88] + 
            self.data[p+72 : p+80] + 
            self.data[p+64 : p+72] + 
            self.data[p+56 : p+64] + 
            self.data[p+48 : p+56] + 
            self.data[p+40 : p+48] + 
            self.data[p+32 : p+40])

      return packet


  def skip_packet(self):
    self.pos += self.frame_size


  def is_packet_valid(self, packet):
    return not (packet["zero_h4_start"].any() or packet["zero_h5_start"].any()
        or packet["zero_h4_end"].any() or packet["zero_t0_start"].any()
        or packet["zero_t0_end"].any())


  def is_next_packet_valid(self):
    packet = self.get_next_packet()
    return self.is_packet_valid(packet)


  # Capture and parse the next packet
  def read_next(self):
      packet = self.get_next_packet()
      
      if not self.is_packet_valid(packet):
          raise ParseError("Frame is not aligned - bits that are expected to be zero are not zero")

      self.skip_packet()

      return packet

