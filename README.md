# WIB data to HDF5 converter

This python script takes data captured by FELIX from WIB from a single optical link. The data is saved to HDF5 format, separating the data by FEMB, column, and ADC.

# How to install this script

```
git clone ssh://git@gitlab.cern.ch:7999/ezhivun/dune-wib-data-parser.git
cd ./dune-wib-data-parser
python3 -m venv venv
source venv/bin/activate
pip3 install --upgrade pip
pip3 install -r requirements.txt
```


# How to capture the data with FELIX

- Load FULL mode firmware to FELIX
- Plug optical fibers to RX ports on FELIX
- run `flx-init`
- check that the optical links are aligned by running `flx-info GBT`
- configure FULL mode data decoding to match with WIB data endianness:

```
flx-config set DECODING_ENDIANNESS_FULL_MODE=0x1
```
- run `elinkconfig` and enable the channels that you want to record, upload the configuration to FELIX
- capture the data. Need to capture data from both logical devices to capture all fiber channels
```
# Capture the data from device 0
fdaqm -t 1 -R -d 0 mydata.dat

# Capture the data from device 1
fdaqm -t 1 -R -d 1 mydata.dat
```
- check the data by running `fcheck <file name>`, make sure there are no error chunks. See the report at the end, which lists elinks that have data (0x00, 0x40, 0x80, etc):
- extract the data from a specific link. One block is equivalent to 32 ns of 
```
# save 20000 1kB blocks of data from elink 0 from the specified file
fcheck -F 20000 -e 0 -w wib_test-221208-172028-1.dat10
```
- `dataout.dat` will now contain the data from an individual link
- rename dataout.dat and use it as the input for this parsing script. Try `-f` flag if the timestamps are not decoded correctly
```
./wib2hdf.py dataout.dat
```
- plot the data (the plot is saved to `dataout_femb0_col0_adc0_chan0.png`)
```
./plot_hdf.py --col 0 --femb 0 --adc 0 --chan 0 ./dataout.dat 
```

The script takes about 2 minutes to decode 1 millisecond of DAQ data.


# Command line arguments

## Data converter
```
$ ./wib2hdf.py -h

usage: wib2hdf.py [-h] [-m MAX_FRAMES] [-s START_OFFSET] [-f] [-o OUTPUT_FILE_NAME] file_name

Decoding DUNE WIB data captured by FELIX and save it into an HDF5 file

positional arguments:
  file_name             file to parse

optional arguments:
  -h, --help            show this help message and exit
  -m MAX_FRAMES, --max_frames MAX_FRAMES
                        Maximum number of frames to decode
  -s START_OFFSET, --start_offset START_OFFSET
                        Offset of the data frame with respect to the beginning of the file in bytes
  -f, --format          Try alternative timestamp format
  -o OUTPUT_FILE_NAME, --output_file_name OUTPUT_FILE_NAME
                        File name for the output HDF5 file
```

## Data plotter
```
$ ./plot_hdf.py -h

usage: plot_hdf.py [-h] [-o OUT_DIR] [-col COL] [-femb FEMB] [-adc ADC] [-ch CHAN] [-n NUM_POINTS] [-t TIME] [-a] [-d] file_name

Plot DUNE wib data from a specified hdf5 file

positional arguments:
  file_name             file to plot

optional arguments:
  -h, --help            show this help message and exit
  -o OUT_DIR, --out_dir OUT_DIR
                        Path to the output directory for the plots
  -col COL, --col COL   FEMB column
  -femb FEMB, --femb FEMB
                        FEMB ID
  -adc ADC, --adc ADC   ADC ID
  -ch CHAN, --chan CHAN
                        ADC channel
  -n NUM_POINTS, --num_points NUM_POINTS
                        Number of points to plot
  -t TIME, --time TIME  How many seconds of data to plot
  -a, --all             Plot all channels (takes a long time, makes many files)
  -d, --deltas          Plot timestamp deltas
```

# Finding the headers

The script will attempt to find the correct data offset automatically while to parsing the file. The frame is found by ensuring certain header bits are all zero. However, if the data contains many zeros the script might not be able to select the correct header offset automatically:

```
$ ./wib2hdf.py -m 1000 ./data/wib_tests_2022_12_12/real_data_no_pulse_link_d0_000.dat -o real_data_no_pulse_link_d0_e00.dat
Looking for WIB headers in ./data/wib_tests_2022_12_12/real_data_no_pulse_link_d0_000.dat
Possible frame header found at [0, 16], trying 0
Creating output file real_data_no_pulse_link_d0_e00.dat
  0%|                   | 1/1000 [00:00<00:02, 360.27frames/s]
Corrupted data in the middle of the file: bits that are expected to be zero are not zero
```

In this case, you can specify the header offset manually using `-s` parameter:
```
$ ./wib2hdf.py -m 1000 ./data/wib_tests_2022_12_12/real_data_no_pulse_link_d0_000.dat -o real_data_no_pulse_link_d0_e00.dat -s 16
Parsing ./data/wib_tests_2022_12_12/real_data_no_pulse_link_d0_000.dat starting at 16
Creating output file real_data_no_pulse_link_d0_e00.dat
100%|███████████████████| 1000/1000 [00:00<00:00, 1437.50frames/s]
```
